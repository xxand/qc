linux里写的一个短小精悍的文件、文件夹备份服务。
目前还有一些输出情况什么的没做的很好，请见谅。
用QT5写的，安装以下。

```
apt install libqt5sql5    #qt5的库
apt install gcc    #gcc
apt install g++    #g++
```
然后把你在本地编译这个程序的编译器文件夹整个拉上服务器

![输入图片说明](https://images.gitee.com/uploads/images/2020/0204/224509_188e3565_856025.png "2020-02-04 22-44-01屏幕截图.png")

编译qt的qmake就在
gcc_64/bin/qmake配置好这个qmake：

```
qmake beifen.pro
```
然后生产一个Makefile

```
make
```
可执行文件就出来了。
把编译的成品拉到需要备份的资源同级目录。

再给三个权限。

```
$  chmod 777 [这软件]
$  chmod 777 [要备份的文件、文件夹]
$  chmod 777 [储存包的文件夹]
$  ./beifen      #运行

```
运行如下
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/170851_5777b767_856025.png "2020-02-08 17-06-38 的屏幕截图.png")

