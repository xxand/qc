#include <QCoreApplication>
#include<QDir>
#include<QDebug>
#include <QProcess>
#include <QTime>
#include <QSettings>
#include <QFile>


bool fileid=1;
QString fileurl_b="/home/beifen/";
QString fileurl_y="./";
QString file_time="1200";
QString file_time2="1200";
         QString tr;
bool g=1;


//延时
void sleepd(int t){/*ms*/
    QTime dieTime = QTime::currentTime().addMSecs(t);
     while( QTime::currentTime() < dieTime )
         QCoreApplication::processEvents(QEventLoop::AllEvents, 100);}

/*检索 某.tar.gz文件*/
QStringList file_bianli(QString path)
{
    QDir dir(path);
    QStringList nameFilters;
    nameFilters << "*.tar.gz" ;
    QStringList files = dir.entryList(nameFilters, QDir::Files|QDir::Readable, QDir::Name);
    return files;
}


/*判断文件夹*/
bool file_jia(QString fullPath)
{
    QDir dir(fullPath);
    if(dir.exists())
    {
      return true;
    }
    return false;
}
/*判断文件*/
bool file(QString f)
{
    QFileInfo fileInfo(f);
    if(fileInfo.isFile())
    {
        return true;
    }
    return false;
}

//创建默认conf



/*检测与创建文件夹*/
/*配置文件*/void conf(){
    bool f=file("./000-core.conf");
    qDebug() <<"\033[0m开始检索配置文件。";
    if(f==true)     qDebug() <<"\033[32m【完成】检索到配置文件。";
    if(f==false)    {qDebug() <<"\033[31m【错误】无法检索到配置文件。";fileid=0;}
}
/*目标文件夹*/void bfile(){

bool f=file_jia(fileurl_y);
qDebug() <<"\033[0m开始检索备份对象。";
if(f==true)
    qDebug() <<"\033[32m【完成】检索到文件、文件夹。";
    if(f==false){
      qDebug()<<"\033[31m【错误】检索失败，请检查配置路径。";fileid=0;}}
/*最终文件夹*/void addfile(){
bool f=file_jia(fileurl_b);
    qDebug() <<"\033[0m开始检索储存文件夹。";
QString tr="mkdir "+fileurl_b;
if(f==true)
    qDebug() <<"\033[32m【完成】检索到文件夹。";
    if(f==false){
      qDebug()<<"\033[31m【错误】检索失败，尝试创建文件夹。";
      QProcess::execute(tr);
            //复查
            f=file_jia(fileurl_b);
            if(f==true){qDebug() <<"\033[32m【完成】已经创建文件夹。";}
            if(f==false){qDebug() <<"\033[31m【失败】无法创建文件夹。";fileid=0;}
    }
}



//取时间
QString t(){//文件名
    QDateTime ti = QDateTime::currentDateTime();
    QString t = ti.toString("yyyyMMdd_hhmmss_zzz");
    return t;

}
QString t2(){//输出信息
    QDateTime ti = QDateTime::currentDateTime();
    QString t = ti.toString("yyyy-MM-dd hh:mm:ss:zzz ddd");
    return t;
}
QString t2_2(){//输出信息
    QDateTime ti = QDateTime::currentDateTime();
    QString t = ti.toString("yyyyMMddhhmmsszzz");
    return t;
}
QString t3(){//输出即时时分
    QDateTime ti = QDateTime::currentDateTime();
    QString t = ti.toString("hhmm");
    return t;
}

//写log
void log(QString ch){
    QSettings *fconf=new QSettings("bf.log",QSettings::IniFormat);
            fconf->beginGroup(t2_2());
            fconf->setValue("LOG",ch);
            fconf->endGroup();
}

//生产备份
void beifen(){


    if(t3()==file_time || t3()==file_time2){//判断时间

        int f=1;
         bfile();//检查文件夹1

         if(fileid==0){qDebug()<<"\033[31m【错误】无法定位备份文件、文件夹。"; f++;}
         addfile();//检查文件夹2
          if(fileid==0){qDebug()<<"\033[31m【错误】无法定位储存文件夹。";f++;}
          if(f>1) {
              qDebug()<<"\033[31m【错误】备份失败："+t2();
               tr="[Error]backup failed "+t2();
               log(tr);
               sleepd(30000);
               g=0;


          }
          //打包了：
          tr="tar -czvf "+fileurl_b+t()+".tar.gz "+fileurl_y;
          QProcess::execute(tr);
         tr="\033[32m【完成】输出完成："+t2()+t()+".tar.gz";
         qDebug()<<tr;
         tr=" [OK]Backup completed "+t2_2()+" fileName: "+t()+".tar.gz";
         log(tr);
         sleepd(30000);



    }

        sleepd(29990);
        qDebug()<<"\033[0m"<<t2()<<"程序正在活动。";
         g=1;

}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug()<<"启动程序：";
    conf();//检查配置
        if(fileid==0){qDebug()<<"\033[0m正在退出。";qDebug()<<"End";return 0;}

     qDebug()<<"\033[0m加载配置文件。";
        QSettings *rconf = new QSettings("000-core.conf",QSettings::IniFormat);
        file_time=rconf->value("set/first_time").toString();
        file_time2=rconf->value("set/second_time").toString();
        fileurl_y=rconf->value("set/backup_file").toString();
        fileurl_b=rconf->value("set/output_file").toString();
                qDebug() <<"\033[0m开始验证配置文件内容。";
        if(file_time==""){qDebug()<<"检测到有必要配置为空，请检查配置。"; sleepd(2000);   qDebug()<<"End";    return 0;}
        if(fileurl_y==""){qDebug()<<"检测到有必要配置为空，请检查配置。"; sleepd(2000);   qDebug()<<"End";    return 0;}
        if(fileurl_b==""){qDebug()<<"检测到有必要配置为空，请检查配置。"; sleepd(2000);   qDebug()<<"End";    return 0;}

    qDebug()<<"\033[32m【完成】已经加载配置文件。";
    bfile();//检查文件夹1
        if(fileid==0){qDebug()<<"\033[0m正在退出。";    sleepd(2000);   qDebug()<<"End";    return 0;}
   addfile();//检查文件夹2
   //not ok；
        if(fileid==0){qDebug()<<"\033[0m正在退出。";    sleepd(2000);   qDebug()<<"End";    return 0;}
   //ok
    //go
    qDebug()<<"\033[0m服务已经正常启动。";
    bool fa=file("bf.log");
    if(fa==false){QProcess::execute("touch ./bf.log");}
    tr="[OK]Start the program normally after checking "+t2() ;
    log(tr);
    beifen();//第一次判断
    while (g==1) {
         beifen();//第二次
        if (g==0) return a.exec();

    }




}
